# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ # Include the results of the hardware scan.
      ./hardware-configuration.nix
    ];

  # Use the GRUB 2 boot loader.
  boot.loader.grub.enable = true;
  boot.loader.grub.version = 2;
  # boot.loader.grub.efiSupport = true;
  # boot.loader.grub.efiInstallAsRemovable = true;
  # boot.loader.efi.efiSysMountPoint = "/boot/efi";
  # Define on which hard drive you want to install Grub.
  boot.loader.grub.device = "/dev/sda"; # or "nodev" for efi only

  boot.initrd.luks.devices.crypted.device = "/dev/disk/by-uuid/fbde1240-c7f1-4ce9-a782-3b51610f1d3b";

  swapDevices = [ { device = "/dev/mapper/nix--lvm-swap"; } ];

  networking.hostName = "Sashas-MacBook-Pro"; # Define your hostname.
  # networking.wireless.enable = true;  # Enables wireless support via wpa_supplicant.

  # Select internationalisation properties.
  # i18n = {
  #   consoleFont = "Lat2-Terminus16";
  #   consoleKeyMap = "us";
  #   defaultLocale = "en_US.UTF-8";
  # }; 

  # Set your time zone.
  time.timeZone = "America/New_York";

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  environment.systemPackages = with pkgs; [ curl wget vim neovim git htop
firefoxWrapper kodi tdesktop weechat keepassx-community qutebrowser neofetch
kdeApplications.spectacle hostsblock kwakd dnsmasq neovim-qt kcalc mosh
vimPlugins.airline vimPlugins.Syntastic unzip p7zip file redshift
redshift-plasma-applet obs-studio krita kdeApplications.okular gwenview
cool-retro-term neovim kdeFrameworks.oxygen-icons5 keybase-gui falkon vlc
kdeApplications.kdenlive ];

  hardware.trackpoint =
    { enable = true;
      emulateWheel = true;
      sensitivity = 128;
      speed = 125;
    };

  # Some programs need SUID wrappers, can be configured further or are
  # started in user sessions.
  # programs.bash.enableCompletion = true;
  # programs.mtr.enable = true;
  # programs.gnupg.agent = { enable = true; enableSSHSupport = true; };
  programs.zsh =
    { enable = true;
      syntaxHighlighting.enable = true;
      ohMyZsh.enable = true;
    };

  programs.tmux =
    { enable = true;
      shortcut = "s";
    };

  # List services that you want to enable:

  # Enable the OpenSSH daemon.
  # services.openssh.enable = true;

  # Open ports in the firewall.
  # networking.firewall.allowedTCPPorts = [ ... ];
  # networking.firewall.allowedUDPPorts = [ ... ];
  # Or disable the firewall altogether.
  # networking.firewall.enable = false;
  networking.networkmanager.enable = true;
  # Enable CUPS to print documents.
  services.printing.enable = true;

  # You hear that?
  hardware.pulseaudio.enable = true;
  # Enable the X11 windowing system.
  services.xserver.enable = true;
  # services.xserver.layout = "us";
  # services.xserver.xkbOptions = "eurosign:e";
  # Enable touchpad support.
  # services.xserver.libinput.enable = true;

  # Enable the KDE Desktop Environment.
  services.xserver.displayManager.sddm.enable = true;
  services.xserver.displayManager.sddm.autoLogin.enable = true;
  services.xserver.displayManager.sddm.autoLogin.user = "pancakes";
  services.xserver.desktopManager.plasma5.enable = true;

  security.hideProcessInformation = true;

  # Define a user account. Don't forget to set a password with ‘passwd’.
  users.defaultUserShell = pkgs.zsh;

  users.extraUsers.pancakes =
    { isNormalUser = true;
      uid = 1000;
      extraGroups = [ "wheel" "video" "audio" "networkmanager" ];
      createHome = true;
      initialPassword = "correct horse battery staple";
      useDefaultShell = true;
    };

  # users.extraUsers.guest = {
  #   isNormalUser = true;
  #   uid = 1000;
  # };

  # This value determines the NixOS release with which your system is to be
  # compatible, in order to avoid breaking some software such as database
  # servers. You should change this only after NixOS release notes say you
  # should.
  system.stateVersion = "17.09"; # Did you read the comment?

}
