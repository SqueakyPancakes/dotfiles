#! /bin/bash

yay -S weechat-vimode-git weechat-matrix-git wee-slack weechat-autosort

mkdir -p ~/.weechat/lua/autoload
ln -s /opt/matrix.lua ~/.weechat/lua
ln -s ~/.weechat/lua/matrix.lua ~/.weechat/lua/autoload

mkdir -p ~/.weechat/python/autoload
ln -s /usr/lib/weechat/python/vimode.py ~/.weechat/python
ln -s ~/.weechat/python/vimode.py ~/.weechat/python/autoload

ln -s /usr/lib/weechat/python/wee_slack.py ~/.weechat/python
ln -s ~/.weechat/python/wee_slack.py ~/.weechat/python/autoload

ln -s /usr/lib/weechat/python/notify_send.py ~/.weechat/python
ln -s ~/.weechat/python/notify_send.py ~/.weechat/python/autoload

ln -s /usr/lib/weechat/python/autosort.py ~/.weechat/python
ln -s ~/.weechat/python/autosort.py ~/.weechat/python/autoload
