# zplug
source ~/.zplug/init.zsh

#zplug "plugins/vi-mode", from:oh-my-zsh
zplug 'lib/completion', from:oh-my-zsh
zplug 'lib/history', from:oh-my-zsh
zplug 'lib/key-bindings', from:oh-my-zsh
zplug 'zsh-users/zsh-syntax-highlighting', defer:2
zplug 'rummik/zsh-vimode-cursor'
#zplug 'rummik/zsh-slowcat'
zplug "woefe/vi-mode.zsh"

zplug load

export EDITOR="nvim"

#history stuff
setopt SHARE_HISTORY
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE
HISTSIZE=10000
SAVEHIST=10000
HISTFILE=~/.cache/zsh/history
#setopt HIST_NO_STORE
HIST_STAMPS="dd/mm/yyyy"

# command autocomplete
#autoload -Uz compinit
#compinit
#zstyle ':completion:*' menu select
#setopt COMPLETE_ALIASES
#zstyle ':completion:*' rehash true

# vi mode without lag
bindkey -v
export KEYTIMEOUT=1

# add missing vim hotkeys
bindkey -a u undo
bindkey -a '^T' redo
bindkey '^?' backward-delete-char  #backspace

# history search in vim mode
# ctrl+r to search history
bindkey -M viins '^r' history-incremental-search-backward
bindkey -M vicmd '^r' history-incremental-search-backward

# combonnation cd and ls
cdls() {
  ls --color
}

add-zsh-hook chpwd cdls

mkcd() {mkdir -p $@; cd $_}

gcd() {git clone $@; cd "$(basename "$_" .git)" }

cast() {bash "youtube-dl -o - $@ | castnow --address 10.17.2.64 --quiet -"}

clone() {git clone http://repo_url.git && cd "$(basename "$_" .git)"}

alias yd="yt-dlp"
alias ydx="yt-dlp -x --audio-format mp3"
alias ydxs="yt-dlp -x --audio-format mp3 --split-chapters"

alias gdl="gallery-dl"
#edit commands

alias ev="$EDITOR ~/.config/nvim/init.vim"
alias ez="$EDITOR ~/.zshrc"
alias em="$EDITOR ~/.muttrc"
alias et="$EDITOR ~/.tmux.conf"
alias emo="$EDITOR ~/Documents/movies"
alias ean="$EDITOR ~/Documents/anime"
alias en="$EDITOR ~/.newsboat/config"
alias ea="$EDITOR ~/git/Autonomous-Garden/Rules.md"

alias st="speedtest"
alias cw="curl -H \"Accept-Language: eo\" wttr.in"
alias cwm="curl -H \"Accept-Language: eo\" wttr.in/moon"
alias mm="cd ~/Music/Misc\\ tracks"

#vagrant
alias vu="vagrant up --provider virtualbox"
alias vs='vagrant ssh -c "cd /vagrant && foreman start"'
alias vd="vagrant suspend"

#netbook
alias ei="$EDITOR ~/.config/i3/config"

#git commands
alias gs='git status '
alias ga='git add '
alias gb='git branch '
alias gc='git commit'
alias gd='git diff'
alias gp='git push'
alias go='git checkout '
alias gk='gitk --all&'
alias gx='gitx --all'

#ls commands
alias ls="ls --color"
alias la="ls -lah --color"

# Please
alias please="sudo !!"

#mosh/ssh commands
alias msb="mosh sunbeam"

#run things as root with your own vimrc and other cool shit.
# alias sudo="sudo -E"

#updates
alias yu="yay -Syu"
alias sau="sudo apt update && sudo apt upgrade"

# history but fun
function hist {
history | awk '{CMD[$2]++;count++;}END { for (a in CMD)print CMD[a] " " CMD[a]/count*100 "% " a;}' | grep -v "./" | column -c3 -s " " -t | sort -nr | nl |  head -n10
}

# Improved tab completion
#autoload -U compinit
#zstyle ':completion:*:*:*:*:globbed-files' matcher 'r:|?=** m:{a-z\-}={A-Z\_}'
#zstyle ':completion:*:*:*:*:local-directories' matcher 'r:|?=** m:{a-z\-}={A-Z\_}'
#zstyle ':completion:*:*:*:*:directories' matcher 'r:|?=** m:{a-z\-}={A-Z\_}'

#zstyle ':completion:*' menu select
#zmodload zsh/complist
# setopt MENU_COMPLETE
#setopt no_list_ambiguous

# Use vim keys in tab complete menu:
bindkey -M menuselect 'h' vi-backward-char
bindkey -M menuselect 'k' vi-up-line-or-history
bindkey -M menuselect 'l' vi-forward-char
bindkey -M menuselect 'j' vi-down-line-or-history
#bindkey -v '^?' backward-delete-char

#fallout style promt for better CRT
PROMPT='$(vi_mode_status) %~>'

VI_NORMAL_MODE_INDICATOR=' %F{blue}[ NORMAL ]%f'
VI_INSERT_MODE_INDICATOR=' %F{green}[ INSERT ]%f'

#slowcat <<< "WELCOME TO ROBCO INDUSTRIES (TM) TERMLINK"
#slowcat <<< ""

source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

#KDE dialogs
export GTK_USE_PORTAL=1
export QT_QPA_PLATFORMTHEME=flatpak

# Path
export  PATH=/home/pancakes/apps:$PATH
