if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

"Vim-plug install stuff
call plug#begin('~/.local/share/nvim/plugged')

Plug 'scrooloose/nerdtree'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
"Plug 'nelstrom/vim-markdown-folding'
Plug 'fneu/breezy'
Plug 'Townk/vim-autoclose'
Plug 'ap/vim-css-color'
"Plug 'tpope/vim-surround'
Plug 'tpope/vim-obsession'
Plug 'roman/golden-ratio'
Plug 'airblade/vim-gitgutter'
"Plug 'mattn/emmet-vim'
Plug 'godlygeek/tabular'
"Plug 'plasticboy/vim-markdown'
"Plug 'vimwiki/vimwiki'

call plug#end()

set background=light
 colorscheme breezy
set termguicolors

"Vundle
filetype on
set nocompatible

"closetag
let g:closetag_filenames = '*.html,*.xhtml,*.phtml'

"Pass file names to tmux tabs
autocmd BufReadPost,FileReadPost,BufNewFile * call system("tmux rename-window " . expand("%:t"))
let tmuxtitle = system("tmux display-message -p '#W'")
"autocmd VimLeave * call system("tmux rename-window " . shellescape(tmuxtitle))
autocmd VimLeave * call system("tmux setw automatic-rename")

"line visibility
set number
set relativenumber
set cul
set scrolloff=3
set conceallevel=2

"useful shit
set ignorecase
set wildignorecase
set smartcase
set wrap
set path+=**
syntax on

"no backups
set nobackup
set nowritebackup
set noswapfile

"Indent options
filetype plugin indent on

set autoindent
set nocindent
set smarttab

"use clipboard
set clipboard=unnamedplus

"CLI options
set title
set mouse=a

"airline stuff
let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
let g:airline#extensions#whitespace#trailing_regexp = '\(\s\)\@<!\s$'
let g:airline_theme='breezy'

set laststatus=2

"Airline theme
"let g:airline_solarized_bg='dark'

"markdown folding
let g:markdown_fold_style = 'nested'
let g:markdown_folding = 1

"Remember line when closing
if has("autocmd")
  au BufReadPost * if line("'\"") > 0 && line("'\"") <= line("$") | exe "normal! g`\"" | endif
endif

"no Qs in tmux
set guicursor=

"run a command and return output
noremap Q !!$SHELL<CR>

"autoremove trailing whitespace
"autocmd BufWritePre * :%s/\s\+$//e

"size of a hard tabstop
set tabstop=2

"always uses spaces instead of tab characters
set expandtab

"size of an "indent"
set shiftwidth=2

"Spellcheck button
map <F6> :setlocal spell! spelllang=en_us<CR>
map <F10> <Esc>:tabnew<CR>

"This unsets the "last search pattern" register by hitting return
nnoremap <CR> :noh<CR><CR>

"show tabs and spaces
set listchars=tab:>-,trail:-
set list

"Make these commonly mistyped commands still work
command! WQ wq
command! Wq wq
command! Wqa wqa
command! W w
command! Q q

"Use :C to clear hlsearch
command! C nohlsearch

"Do the splits
set splitbelow
set splitright
set fillchars+=vert:│

"NERDtree sutff
map <C-n> :NERDTreeToggle<CR>
"Nerdtree auto start in directory
autocmd StdinReadPre * let s:std_in=1
autocmd vimenter * silent! lcd %:p:h
"autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif
"close nerdtree if only nerdtree is open
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

"80 colum line
highlight ColorColum ctermbg=magenta
call matchadd('ColorColum', '\%81v', 100)

"Tab completion of words.
function! Tab_Or_Complete()
  if col('.')>1 && strpart( getline('.'), col('.')-2, 3 ) =~ '^\w'
    return "\<C-N>"
  else
    return "\<Tab>"
  endif
endfunction
:inoremap <Tab> <C-R>=Tab_Or_Complete()<CR>
:set dictionary="/usr/dict/words"
