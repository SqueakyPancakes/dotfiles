#! /bin/bash

##install dotfiles and shit


mkdir ~/.ssh;
mkdir -p ~/.config/htop;
mkdir -p ~/.newsboat;

ln -s `pwd`/.config/htop/htoprc ~/.conig/htop/htoprc;
ln -s `pwd`/.zshrc ~/.zshrc;
ln -s `pwd`/nvim/init.vim ~/.config/nvim/init.vim;
ln -s `pwd`/nvim/colors ~/.config/nvim/colors;
ln -s `pwd`/.urlview ~/.urlview;
ln -s `pwd`/.newsboat/config ~/.newsboat/config;
# makes markdown viewer work
ln -s `pwd`/.mime.types ~/.mime.types;

git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm;
curl -sL --proto-redir -all,https https://raw.githubusercontent.com/zplug/installer/master/installer.zsh | zsh;
curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
