
# List of plugins
set -g @plugin 'tmux-plugins/tpm'
set -g @plugin 'tmux-plugins/tmux-sensible'
set -g @plugin 'tmux-plugins/tmux-resurrect'
set -g @plugin 'tmux-plugins/tmux-continuum'
set -g @plugin 'tmux-plugins/tmux-urlview'

# resurrection stuff
set -g @resurrect-save 'S'
set -g @resurrect-restore 'O'
set -g @resurrect-strategy-nvim 'session'

# automatic restore
set -g @continuum-restore 'on'
set -g @continuum-save-interval '10'

# use C-s, since it's on the home row and easier to hit than C-b
set-option -g prefix C-s
unbind-key C-s
bind-key C-s send-prefix
set -g base-index 1

# Easy config reload
bind-key R source-file ~/.tmux.conf \; display-message "tmux.conf reloaded."

set -ga terminal-overrides ',xterm-256color:Tc'

# vi is good
setw -g mode-keys vi

# mouse behavior
#set -g mouse-utf8 on
set -g mouse on

# resize window base on client
setw -g aggressive-resize on

bind-key : command-prompt
bind-key r refresh-client
bind-key L clear-history

bind-key enter next-layout

# use vim-like keys for splits and windows
bind-key v split-window -h -c "#{pane_current_path}"
bind-key s split-window -v -c "#{pane_current_path}"
bind-key h select-pane -L
bind-key j select-pane -D
bind-key k select-pane -U
bind-key l select-pane -R

# smart pane switching with awareness of vim splits
bind -n C-h run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim$' && tmux send-keys C-h) || tmux select-pane -L"
bind -n C-j run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim$' && tmux send-keys C-j) || tmux select-pane -D"
bind -n C-k run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim$' && tmux send-keys C-k) || tmux select-pane -U"
bind -n C-l run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim$' && tmux send-keys C-l) || tmux select-pane -R"
bind -n C-\ run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim$' && tmux send-keys 'C-\\') || tmux select-pane -l"
bind C-l send-keys 'C-l'

bind-key C-o rotate-window

bind-key + select-layout main-horizontal
bind-key = select-layout main-vertical
set-window-option -g other-pane-height 25
set-window-option -g other-pane-width 80

bind-key a last-pane
bind-key q display-panes
bind-key c new-window
bind-key -n C-k next-window
bind-key -n C-j previous-window

bind-key [ copy-mode
bind-key ] paste-buffer

set-window-option -g display-panes-time 1500

set-option -g renumber-windows on

# precent window renaming
set-window-option -g automatic-rename on
set-option -g allow-rename off

# remove delay in neovim mode changes
set -sg escape-time 0

# status line
set -g status-justify centre
set -g status-interval 2

# loud or quiet?
set-option -g visual-activity on
set-option -g visual-bell on
set-option -g visual-silence off
set-window-option -g monitor-activity off
set-option -g bell-action none

#colour0 (black)
#colour1 (red)
#colour2 (green)
#colour3 (yellow)
#colour4 (blue)
#colour7 (white)
#colour5 colour6 colour7 colour8 colour9 colour10 colour11 colour12 colour13 colour14 colour15 colour16 colour17

# Basic status bar colors
set -g status-fg colour240
set -g status-bg colour237

# Left side of status bar
set -g status-left-bg color240
set -g status-left-fg color237
set -g status-left-length 40
set -g status-left "#[fg=colour248,bg=colour0] #H #[fg=colour248,bg=colour235,nobold]#[fg=colour0,bg=colour237]"

# Right side of status bar
set -g status-right-bg colour233
set -g status-right-fg colour243
set -g status-right-length 150
set -g status-right "#[fg=colour235,bg=colour237]#[fg=colour245,bg=colour235] CS:#{continuum_status} #[fg=colour24,bg=colour235]#[fg=colour250,bg=colour24, bold] %d/%m #[fg=colour250,bg=colour24,nobold] %H:%M:%S "

# Window status
set -g window-status-format "#[fg=colour237,bg=colour237]#[fg=colour248,bg=colour237]#I#F #W "
set -g window-status-current-format "#[fg=colour237,bg=colour24]#[fg=colour255,bg=colour24] #I#F #W #[fg=colour24,bg=colour237,nobold]"

# Current window status
set -g window-status-current-bg colour24
set -g window-status-current-fg colour255

# Window with activity status ?????notificaions
set -g window-status-activity-bg colour3
set -g window-status-activity-fg colour237

# Window separator
set -g window-status-separator ""

# Pane border
set-option -g pane-border-style fg=colour235
set-option -g pane-active-border-style fg=colour4

# Pane number indicator
set -g display-panes-colour colour233
set -g display-panes-active-colour colour245

# Clock mode
set -g clock-mode-colour colour24
set -g clock-mode-style 24

# Message
set -g message-bg colour1
set -g message-fg white

# Command message
set -g message-command-bg colour3
set -g message-command-fg black

# Mode
set -g mode-bg colour24
set -g mode-fg colour232

run '~/.tmux/plugins/tpm/tpm'
bind-key -n C-l if-shell "$is_vim" "send-keys C-l"  "send-keys C-l"
