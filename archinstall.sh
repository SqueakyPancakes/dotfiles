#!/bin/bash

sudo pacman -S neovim konsole plasma-desktop krita mosh dolphin neofetch zsh kde-gtk-config vlc ark filelight kcalc okular spectacle sddm-kcm plasma-nm kscreen speedtest-cli kup kcm-wacomtablet gwenview plasma-pa keepassxc mutt kcm-wacomtablet bup zsh-syntax-highlighting unrar p7zip

chsh -s $(which zsh)

mkdir git
cd git
git clone https://aur.archlinux.org/yay.git
cd yay
makepkg -si

yay -S oh-my-zsh-plugin-autosuggestions
yay -S zsh-fast-syntax-highlighting

